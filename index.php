<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/css/bootstrap.min.css' media="screen" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" type="text/css" />
  <link rel="stylesheet" href="theme.css" type="text/css">
</head>
<title>Register</title>
<body>
  <?php
  // validate date format dd/MM/yyyy
  function isDate($string)
  {
    $regex = '/^([0-9]{1,2})\\/([0-9]{1,2})\\/([0-9]{4})$/';
    if (!preg_match($regex, $string)) return false;
    return true;
  }
  // Code PHP xử lý validate
  $error = array();
  if ($_SERVER["REQUEST_METHOD"] == "GET") {

    // Kiểm tra định dạng dữ liệu
    if (empty($_GET['fullName'])) {
      $error['fullName'] = 'Hãy nhập tên.';
    }

    if (empty($_GET['gender'])) {
      $error['gender'] = 'Hãy chọn giới tính.';
    }
    if (empty($_GET['khoa'])) {
      $error['khoa'] = 'Hãy chọn phân khoa.';
    }
    if (empty($_GET['birthday'])) {
      $error['birthday'] = 'Hãy nhập ngày sinh.';
    }elseif(!isDate($_GET['birthday'])) {
      $error['birthdayFormat'] = 'Hãy nhập ngày sinh đúng định dạng.';
    }
  }
  ?>
  <div class="container">
      <form action="index.php" method="GET" id="form">
        <?php
        if ($error) {
          foreach ($error as $key => $value) {
            echo '
                <p style="color: red">' . $value . '</p>
              ';
          }
        }
        ?>
        <div class="user_input">
          <lable>Họ và tên
            <span style="color: red">*</span>
          </lable>
          <input type="text" name="fullName" id="fullName">
        </div>

        <div class="user_input">
          <lable>Giới tính
            <span style="color: red">*</span>
          </lable>
            <?php
            $gender = array('0' => 'Nam', '1' => 'Nữ');
            for ($i = 0; $i < count($gender); $i++) {
              echo '
                <input type="radio" id="gender" name="gender" value="' . $gender[$i] . '">
                ';
              echo '
                <label>' . $gender[$i] . '</label> 
                ';
            }
            ?>
        </div>

        <div class="user_input">
          <lable>Phân khoa
            <span style="color: red">*</span>
          </lable>
            <select id="khoa" name="khoa" id="khoa" style="height: inherit">
              <?php
              $khoa = array(
                "0" => "",
                "MAT" => "Khoa học máy tính",
                "KDL" => "Khoa học vật liệu",
              );
              
              foreach ($khoa as &$value) {
                echo '<option id = "khoa" value="' . $value . '">' . $value .'</option>';
              }
              ?>
            </select>
        </div>

        <div class="user_input" date-date-format="dd/MM/yyyy">
          <lable>Ngày sinh
            <span style="color: red">*</span>
          </lable>
          <input type="text" name="birthday" id="birthday" placeholder="dd/mm/yyyy">
        </div>

        <div class="user_input">
          <lable>Địa chỉ</lable>
          <input type="text" name="address" id="address">
        </div>
        <div class ="user_submit">
          <input type="submit" name="btnSubmit" id="form_btn" value="Đăng ký">
        </div>

      </form>
  </div>
  <?php
  ?>
  <script type="text/javascript" src='https://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.8.3.min.js'></script>
  <script type="text/javascript" src='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/js/bootstrap.min.js'></script>
  <!-- Bootstrap -->
  <!-- Bootstrap DatePicker -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js" type="text/javascript"></script>
  <!-- Bootstrap DatePicker -->
  <script type="text/javascript">
    $(function() {
      $('#birthday').datepicker({
        format: "dd/mm/yyyy"
      });
    });
  </script>
  
</body>

</html>